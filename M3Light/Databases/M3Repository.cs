﻿using System.Data;
using System.Data.SqlClient;

namespace M3Light.Databases {
    public class M3Repository {
        public DataTable GetListItems (SqlConnection conn, string facility, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListItems] '{0}', '{1}'", facility.Trim(), sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListItems", conn);
        }

        public DataTable GetFacilitiesByUser (SqlConnection conn, string username) {
            var query = string.Format(@"SELECT * FROM [dbo].[M3LightUserFacilityView] WHERE RTRIM(LTRIM(LOWER(Username)))='{0}'", username.ToLower());

            return PolisportAPI.Data.Database.QueryReader(query, "GetFacilities", conn);
        }

        public DataTable GetListItemsWarehouse (SqlConnection conn, string artigo, string facilities, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListItemsWarehouse] '{0}', '{1}', '{2}'", artigo.Trim().ToLower(), facilities, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListItemsWarehouse", conn);
        }

        public DataTable GetListClients (SqlConnection conn, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListClients] '{0}'", sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListClients", conn);
        }

        public DataTable GetListStock (SqlConnection conn, string artigo, string armazém, string facilities, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListStock] '{0}', '{1}', '{2}','{3}'", artigo.Trim().ToLower(), armazém.Trim().ToLower(), facilities, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListStock", conn);
        }

        public DataTable GetListMaterialPlan (SqlConnection conn, string artigo, string armazém, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListMaterialPlan] '{0}', '{1}', '{2}'", artigo.Trim().ToLower(), armazém.Trim().ToLower(), sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListMaterialPlan", conn);
        }

        public DataTable GetListStockTransactions (SqlConnection conn, string artigo, string armazém, string firstDate, string lastDate, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListStockTransactions] '{0}', '{1}', '{2}', '{3}', '{4}'", artigo.Trim().ToLower(), armazém.Trim().ToLower(), firstDate, lastDate, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListStockTransactions", conn);
        }

        public DataTable GetListCustomerOrder (SqlConnection conn, string facility, string firstDate, string lastDate, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListCustomerOrder] '{0}', '{1}', '{2}', '{3}'", facility.Trim().ToLower(), firstDate, lastDate, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListCustomerOrder", conn);
        }

        public DataTable GetListSalesPriceList (SqlConnection conn, string listaPreço, string cliente, string firstDate, string lastDate, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListSalesPriceList] '{0}', '{1}', '{2}', '{3}', '{4}'", listaPreço.Trim().ToLower(), cliente.Trim().ToLower(), firstDate, lastDate, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListSalesPriceList", conn);
        }

        public DataTable GetListCosting (SqlConnection conn, string artigo, string facility, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListCosting] '{0}', '{1}', '{2}'", artigo.Trim().ToLower(), facility.Trim().ToLower(), sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListCosting", conn);
        }

        public DataTable GetListCostingByModelCost (SqlConnection conn, string artigo, string facility, string tipoCusteio, string data, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListCostingByModelCost] '{0}', '{1}', '{2}', '{3}', '{4}'", artigo.Trim().ToLower(), facility.Trim().ToLower(), tipoCusteio, data, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListCostingByModelCost", conn);
        }

        public DataTable GetListMaterialOperation (SqlConnection conn, string artigo, string facility, string tipoCusteio, string data, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListCostingMaterialOperation] '{0}', '{1}', '{2}', '{3}', '{4}'", artigo.Trim().ToLower(), facility.Trim().ToLower(), tipoCusteio, data, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListMaterialOperation", conn);
        }

        public DataTable GetListItemMasterFile (SqlConnection conn, string artigo, string armazem, string isYear, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListItemMasterFile] '{0}', '{1}', '{2}', '{3}'", armazem.Trim().ToLower(), artigo.Trim().ToLower(), isYear, sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListItemMasterFile", conn);
        }

        public DataSet GetListItemDiscontinuation (SqlConnection conn, string artigo, string facility) {
            var query = string.Format(@"EXEC [dbo].[M3LightPDC] '{0}','{1}'", artigo.Trim().ToLower(), facility.Trim().ToLower());

            return PolisportAPI.Data.Database.QueryReaderDS(query, "GetListItemDiscontinuation", conn);
        }

        public DataTable GetListItemAlias (SqlConnection conn, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListItemAlias] '{0}'", sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListItemAlias", conn);
        }

        public DataTable GetListSuppliers (SqlConnection conn, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListSuppliers] '{0}'", sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListSuppliers", conn);
        }

        public DataTable GetListItemSupplier (SqlConnection conn, string artigo, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListItemSupplier] '{0}','{1}'", artigo.Trim().ToLower(), sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListItemSupplier", conn);
        }

        public DataTable GetListManufacturingOrder (SqlConnection conn, string facility, string sqlSelect) {
            var query = string.Format(@"EXEC [dbo].[M3LightListManufacturingOrder] '{0}','{1}'", facility.Trim().ToLower(), sqlSelect);

            return PolisportAPI.Data.Database.QueryReader(query, "GetListManufacturingOrder", conn);
        }
    }
}