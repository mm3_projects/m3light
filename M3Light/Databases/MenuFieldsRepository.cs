﻿using M3Light.Models;
using PolisportAPI.Data.Repository;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace M3Light.Databases {
    public class MenuFieldsRepository : IRepository {
        public string TableName {
            get {
                return "[dbo].[M3Light_MenuFields]";
            }
        }

        public List<MenuFieldsPoco> GetMenuFields (SqlConnection conn, int menuID) {
            var query = string.Format(@"SELECT * FROM {0} WHERE [MenuID]='{1}'", TableName, menuID);

            return PolisportAPI.Data.Database.Query<MenuFieldsPoco>(query, conn);
        }
    }
}