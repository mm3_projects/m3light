﻿using M3Light.Models;
using PolisportAPI.Data.Repository;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace M3Light.Databases {
    public class UserMenuPermissionViewRepository : IRepository {
        public string TableName {
            get {
                return "[dbo].[M3LightUserMenuPermissionView]";
            }
        }

        public List<UserPermissionViewPoco> GetMenus (SqlConnection conn, string username) {
            var query = string.Format(@"SELECT * 
                        FROM {0}
                        WHERE RTRIM(LTRIM(LOWER([Username]))) = '{1}'", TableName, username.Trim().ToLower());

            return PolisportAPI.Data.Database.Query<UserPermissionViewPoco>(query, conn);
        }
    }
}