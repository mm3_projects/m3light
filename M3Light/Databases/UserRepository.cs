﻿using M3Light.Models;
using PolisportAPI.Data.Repository;
using System.Data.SqlClient;

namespace M3Light.Databases
{
    public class UserRepository : IRepository
    {
        public string TableName {
            get {
                return "[dbo].[M3Light_User]";
            }
        }

        public UserPoco GetUser(SqlConnection conn, string username)
        {
            var query = string.Format(@"SELECT * FROM {0} WHERE [IsEnable]=1 and RTRIM(LTRIM(LOWER([Username])))='{1}'", TableName, username.Trim().ToLower());

            return PolisportAPI.Data.Database.QueryRow<UserPoco>(query, conn);
        }
    }
}