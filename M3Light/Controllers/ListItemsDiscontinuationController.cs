﻿using M3Light.Logic;
using M3Light.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace M3Light.Controllers {
    public class ListItemsDiscontinuationController : Controller {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        GridLogic gridLogic = new GridLogic();
        #endregion

        public ActionResult Index () {
            #region CheckSessions
            if (Session[CacheSystem.SessionNameUsername] == null ||
               Session[CacheSystem.SessionNameMenus] == null ||
               Session[CacheSystem.SessionNameMenuID] == null ||
               Session[CacheSystem.SessionNameProcessDataModel] == null) {
                return RedirectToAction("Index", "Login");
            }

            var menus = (List<UserPermissionViewPoco>)Session[CacheSystem.SessionNameMenus];
            var menuInfo = menus
                .Where(x => x.Controller.ToLower().Trim() == this.ControllerContext.RouteData.Values["controller"].ToString().ToLower().Trim())
                .FirstOrDefault();

            if (menuInfo.MenuID != int.Parse(Session[CacheSystem.SessionNameMenuID].ToString())) {
                return RedirectToAction("ProcessDataMenu", "Core", new { menuID = int.Parse(Session[CacheSystem.SessionNameMenuID].ToString()), refreshData = true });
            }
            #endregion

            var model = Session[CacheSystem.SessionNameProcessDataModel] as ProcessDataModel;

            if (model.RequiredFilters.Count == 0) {
                return View(new DataSetTableModel());
            }

            var artigo = model.RequiredFilters["artigo"];
            var facility = model.RequiredFilters["facility"];

            var data = m3Logic.GetListItemDiscontinuation(artigo, facility);


            return View(new DataSetTableModel() {
                DataToShow = data,
                RequiredFilters = model.RequiredFilters
            });
        }
    }
}