﻿using M3Light.Logic;
using M3Light.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace M3Light.Controllers {
    public class ListStockTransactionsController : Controller {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        GridLogic gridLogic = new GridLogic();
        #endregion

        public ActionResult Index () {
            #region CheckSessions
            if (Session[CacheSystem.SessionNameUsername] == null ||
               Session[CacheSystem.SessionNameMenus] == null ||
               Session[CacheSystem.SessionNameMenuID] == null ||
               Session[CacheSystem.SessionNameProcessDataModel] == null) {
                return RedirectToAction("Index", "Login");
            }

            var menus = (List<UserPermissionViewPoco>)Session[CacheSystem.SessionNameMenus];
            var menuInfo = menus
                .Where(x => x.Controller.ToLower().Trim() == this.ControllerContext.RouteData.Values["controller"].ToString().ToLower().Trim())
                .FirstOrDefault();

            if (menuInfo.MenuID != int.Parse(Session[CacheSystem.SessionNameMenuID].ToString())) {
                return RedirectToAction("ProcessDataMenu", "Core", new { menuID = int.Parse(Session[CacheSystem.SessionNameMenuID].ToString()), refreshData = true });
            }
            #endregion

            var model = Session[CacheSystem.SessionNameProcessDataModel] as ProcessDataModel;

            if (model.RequiredFilters.Count == 0) {
                return View(new GridTableModel());
            }

            var dataSession = Session[CacheSystem.SessionNameAllData] as AllDataModel;

            if (model.RefreshData) {
                var requiredFilters = new List<string> { "artigo", "data registo", "armazém" };

                var artigo = model.RequiredFilters["artigo"];
                var armazém = model.RequiredFilters["armazém"];
                var dataRegisto = model.RequiredFilters["data registo"].Split(new string[] { "<->" }, StringSplitOptions.None);
                var firstDate = dataRegisto[0].Replace("-", "");
                var lastDate = dataRegisto[1].Replace("-", "");

                dataSession.Data = m3Logic.GetListStockTransactions(artigo, armazém, firstDate, lastDate, string.Join(",", model.DatabaseMandatoryColumns));
                Session[CacheSystem.SessionNameAllData] = dataSession;

                var removeFilters = model.RequiredFilters.Keys.Where(x => !requiredFilters.Contains(x)).ToList();
                foreach (var filter in removeFilters) {
                    model.RequiredFilters.Remove(filter);
                }
            }

            var gridModel = gridLogic.FillData(dataSession.Data, model, new List<SubMenuModel>());

            Session[CacheSystem.SessionNamePartialData] = new AllDataModel() {
                Data = gridModel.AllPartialData,
                ColumnsToShow = model.ColumnsToShow
            };

            return View(gridModel);
        }
    }
}