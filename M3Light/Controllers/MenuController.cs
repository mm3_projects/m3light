﻿using M3Light.Logic;
using System.Web.Mvc;

namespace M3Light.Controllers {
    public class MenuController : Controller {
        public ActionResult Index () {
            if (Session[CacheSystem.SessionNameUsername] == null || Session[CacheSystem.SessionNameMenus] == null) {
                return RedirectToAction("Index", "Login");
            }

            return View();
        }
    }
}