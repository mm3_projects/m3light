﻿using M3Light.Logic;
using M3Light.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace M3Light.Controllers {
    public class ListItemsWarehouseController : Controller {
        #region dependencies
        M3Logic m3Logic = new M3Logic();
        GridLogic gridLogic = new GridLogic();
        #endregion

        public ActionResult Index () {
            #region CheckSessions
            if (Session[CacheSystem.SessionNameUsername] == null ||
               Session[CacheSystem.SessionNameMenus] == null ||
               Session[CacheSystem.SessionNameMenuID] == null ||
               Session[CacheSystem.SessionNameProcessDataModel] == null) {
                return RedirectToAction("Index", "Login");
            }

            var menus = (List<UserPermissionViewPoco>)Session[CacheSystem.SessionNameMenus];
            var menuInfo = menus
                .Where(x => x.Controller.ToLower().Trim() == this.ControllerContext.RouteData.Values["controller"].ToString().ToLower().Trim())
                .FirstOrDefault();

            if (menuInfo.MenuID != int.Parse(Session[CacheSystem.SessionNameMenuID].ToString())) {
                return RedirectToAction("ProcessDataMenu", "Core", new { menuID = int.Parse(Session[CacheSystem.SessionNameMenuID].ToString()), refreshData = true });
            }
            #endregion

            var model = Session[CacheSystem.SessionNameProcessDataModel] as ProcessDataModel;

            if (model.RequiredFilters.Count == 0) {
                return View(new GridTableModel());
            }

            var subMenus = new List<SubMenuModel>();
            if (model.SubMenus.Count > 0) {
                var listFilters = new List<string>() { "artigo", "armazém", };

                foreach (var subMenu in model.SubMenus) {
                    subMenus.Add(new SubMenuModel() {
                        MenuID = subMenu.MenuID,
                        MenuName = subMenu.Menu,
                        Filters = listFilters
                    });
                }
            }

            var dataSession = (AllDataModel)Session[CacheSystem.SessionNameAllData];

            if (model.RefreshData) {
                var requiredFilters = new List<string> { "artigo" };

                var artigo = model.RequiredFilters["artigo"];
                var facilites = (Dictionary<string, string>)Session[CacheSystem.SessionNameFacilities];

                dataSession.Data = m3Logic.GetListItemsWarehouse(artigo, "''" + string.Join("'',''", facilites.Values) + "''", string.Join(",", model.DatabaseMandatoryColumns));
                Session[CacheSystem.SessionNameAllData] = dataSession;

                var removeFilters = model.RequiredFilters.Keys.Where(x => !requiredFilters.Contains(x)).ToList();
                foreach (var filter in removeFilters) {
                    model.RequiredFilters.Remove(filter);
                }
            }

            var gridModel = gridLogic.FillData(dataSession.Data, model, subMenus);

            Session[CacheSystem.SessionNamePartialData] = new AllDataModel() {
                Data = gridModel.AllPartialData,
                ColumnsToShow = model.ColumnsToShow
            };

            return View(gridModel);
        }
    }
}