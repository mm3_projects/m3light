﻿using M3Light.Logic;
using System.Web.Mvc;

namespace M3Light.Controllers {
    public class LoginController : Controller {
        #region dependencies
        LoginLogic loginLogic = new LoginLogic();
        MenuLogic menuLogic = new MenuLogic();
        M3Logic m3Logic = new M3Logic();
        #endregion

        public ActionResult Index () {
            ViewBag.Error = Session["LoginErrorMessage"];
            Session["LoginErrorMessage"] = null;

            return View();
        }

        public ActionResult Login (string username, string password) {
            if (!loginLogic.CheckLogin(username, password)) {
                Session["LoginErrorMessage"] = "Login Inválido!";
                return RedirectToAction("Index", "Login");
            }

            Session[CacheSystem.SessionNameUsername] = username;
            Session[CacheSystem.SessionNameMenus] = menuLogic.GetMenus(username);
            Session[CacheSystem.SessionNameFacilities] = m3Logic.GetFacilities(Session[CacheSystem.SessionNameUsername].ToString());

            return RedirectToAction("Index", "Menu");
        }

        public ActionResult Logout () {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}