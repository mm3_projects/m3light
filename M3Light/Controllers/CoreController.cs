﻿using M3Light.Logic;
using M3Light.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;


namespace M3Light.Controllers {
    public class CoreController : Controller {
        #region dependencies
        GridLogic gridLogic = new GridLogic();
        MenuLogic menuLogic = new MenuLogic();
        #endregion

        [HttpGet]
        public void Export (string sessionName) {
            #region Parâmetros globais que devem ser validados em todos os menus
            if (Session[CacheSystem.SessionNameUsername] == null || Session[CacheSystem.SessionNameMenus] == null ||
                Session[CacheSystem.SessionNameMenuID] == null || Session[CacheSystem.SessionNameAllData] == null) {
                RedirectToAction("Index", "Login");
                return;
            }
            #endregion

            if (sessionName == null || Session[sessionName] == null) {
                return;
            }

            var sessionData = ((AllDataModel)Session[sessionName]);
            var workbook = gridLogic.Export(sessionData.Data, sessionData.ColumnsToShow);

            var fileName = string.Format("M3Light_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmss"));

            using (var exportData = new MemoryStream()) {
                Response.Clear();
                workbook.Write(exportData);
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
                Response.BinaryWrite(exportData.ToArray());
                Response.End();
            }
        }

        public ActionResult ProcessDataMenu (int menuID = 0, int page = 1, string sort = "", string sortDir = "", bool refreshData = false, string filters = "", string requiredFilters = "", string addColumns = "", bool isSubMenu = false) {
            #region SessionsCheck
            if (Session[CacheSystem.SessionNameUsername] == null ||
                Session[CacheSystem.SessionNameMenus] == null ||
                (Session[CacheSystem.SessionNameMenuID] == null && menuID == 0)) {
                return RedirectToAction("Index", "Login");
            }

            var cleanFilters = false;

            if (menuID == 0 && Session[CacheSystem.SessionNameMenuID] != null) {
                menuID = int.Parse(Session[CacheSystem.SessionNameMenuID].ToString());
            } else {
                if (Session[CacheSystem.SessionNameMenuID] != null && Session[CacheSystem.SessionNameMenuID].ToString() != menuID.ToString()) {
                    cleanFilters = true;
                }

                Session[CacheSystem.SessionNameMenuID] = menuID;
            }
            #endregion

            var allDataModel = Session[CacheSystem.SessionNameAllData] as AllDataModel;

            if (allDataModel == null) {
                refreshData = true;
                allDataModel = new AllDataModel();
            }

            #region MenuFields
            var menuFields = menuLogic.GetMenuFields(menuID);

            var menuListToShow = menuFields
               .Where(x => x.IsMandatory == true)
               .Select(x => x.Alias);

            var databaseMandatoryColumns = menuFields
                .OrderByDescending(x => x.IsMandatory)
                .Select(x => gridLogic.GetColumnName(x.Field, x.Alias));

            var addColumnsDic = menuFields
               .Where(x => x.IsMandatory == false)
               .ToDictionary(x => x.Alias, x => x.Description);

            if (!string.IsNullOrEmpty(addColumns)) {
                var newColumnsToShow = menuFields
                  .Where(x => !string.IsNullOrEmpty(addColumns) && addColumns.Trim().Contains(x.Alias.Trim()))
                  .Select(x => x.Alias);

                menuListToShow = menuListToShow.Union(newColumnsToShow);
            }
            #endregion

            allDataModel.ColumnsToShow = menuListToShow.ToList();
            Session[CacheSystem.SessionNameAllData] = allDataModel;

            if (cleanFilters) {
                filters = string.Empty;
                page = 1;
                sort = string.Empty;
                sortDir = string.Empty;
                addColumns = string.Empty;
            }

            var model = new ProcessDataModel() {
                RefreshData = refreshData,
                ColumnsToShow = menuListToShow.ToList(),
                DatabaseMandatoryColumns = databaseMandatoryColumns.ToList(),
                Filters = string.IsNullOrEmpty(filters) ? new Dictionary<string, string>() : JsonConvert.DeserializeObject<Dictionary<string, string>>(filters),
                MenuFieldsSelected = addColumns,
                MenuFieldsToAdd = addColumnsDic,
                Page = page,
                RequiredFilters = string.IsNullOrEmpty(requiredFilters) ? new Dictionary<string, string>() : JsonConvert.DeserializeObject<Dictionary<string, string>>(requiredFilters),
                Sort = sort,
                SortDir = sortDir,
                SubMenus = ((List<UserPermissionViewPoco>)Session[CacheSystem.SessionNameMenus]).Where(x => x.MenuPrincipalID.HasValue && x.MenuPrincipalID == menuID).ToList(),
                IsSubMenu = isSubMenu
            };

            Session[CacheSystem.SessionNameProcessDataModel] = model;
            var menuInfo = ((List<UserPermissionViewPoco>)Session[CacheSystem.SessionNameMenus]).Where(x => x.MenuID == menuID).FirstOrDefault();

            return RedirectToAction(menuInfo.Action, menuInfo.Controller);
        }
    }
}