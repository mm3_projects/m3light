﻿using M3Light.Databases;
using PolisportAPI;
using System.Collections.Generic;
using System.Data;

namespace M3Light.Logic {
    public class M3Logic {
        #region dependencies
        M3Repository m3Repo = new M3Repository();
        #endregion

        public DataTable GetListItems (string facility, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListItems(conn, facility, sqlSelect);
            }
        }

        public Dictionary<string, string> GetFacilities (string username) {
            var data = new DataTable();
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                data = m3Repo.GetFacilitiesByUser(conn, username);
            }

            var dic = new Dictionary<string, string>();
            foreach (DataRow facility in data.Rows) {
                dic.Add(facility["CFFACN"].ToString().Trim(), facility["CFFACI"].ToString().Trim());
            }

            return dic;
        }

        public DataTable GetListItemsWarehouse (string artigo, string facilities, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListItemsWarehouse(conn, artigo, facilities, sqlSelect);
            }
        }

        public DataTable GetListClients (string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListClients(conn, sqlSelect);
            }
        }

        public DataTable GetListStock (string artigo, string armazém, string facilities, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListStock(conn, artigo, armazém, facilities, sqlSelect);
            }
        }

        public DataTable GetListMaterialPlan (string artigo, string armazém, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListMaterialPlan(conn, artigo, armazém, sqlSelect);
            }
        }

        public DataTable GetListStockTransactions (string artigo, string armazém, string firstDate, string lastDate, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListStockTransactions(conn, artigo, armazém, firstDate, lastDate, sqlSelect);
            }
        }

        public DataTable GetListCustomerOrder (string facility, string firstDate, string lastDate, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListCustomerOrder(conn, facility, firstDate, lastDate, sqlSelect);
            }
        }

        public DataTable GetListSalesPriceList (string listaPreço, string cliente, string firstDate, string lastDate, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListSalesPriceList(conn, listaPreço, cliente, firstDate, lastDate, sqlSelect);
            }
        }

        public DataTable GetListCosting (string artigo, string facility, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListCosting(conn, artigo, facility, sqlSelect);
            }
        }

        public DataTable GetListCostingByModelCost (string artigo, string facility, string tipoCusteio, string data, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListCostingByModelCost(conn, artigo, facility, tipoCusteio, data, sqlSelect);
            }
        }

        public DataTable GetListCostingMaterialOperation (string artigo, string facility, string tipoCusteio, string data, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListMaterialOperation(conn, artigo, facility, tipoCusteio, data, sqlSelect);
            }
        }

        public DataTable GetListItemMasterFile (string artigo, string armazem, string isYear, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListItemMasterFile(conn, artigo, armazem, isYear, sqlSelect);
            }
        }

        public DataSet GetListItemDiscontinuation (string artigo, string facility) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListItemDiscontinuation(conn, artigo, facility);
            }
        }

        public DataTable GetListItemAlias (string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListItemAlias(conn, sqlSelect);
            }
        }

        public DataTable GetListSuppliers (string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListSuppliers(conn, sqlSelect);
            }
        }

        public DataTable GetListItemSupplier (string artigo, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListItemSupplier(conn, artigo, sqlSelect);
            }
        }

        public DataTable GetListManufacturingOrder (string facility, string sqlSelect) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return m3Repo.GetListManufacturingOrder(conn, facility, sqlSelect);
            }
        }
    }
}