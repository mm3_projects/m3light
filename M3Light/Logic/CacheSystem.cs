﻿namespace M3Light.Logic {
    public static class CacheSystem {
        public static string SoftwareTitle = "M3Light";

        public static string SessionNameAllData = "AllData";
        public static string SessionNamePartialData = "PartialData";
        public static string SessionNameMenus = "Menus";
        public static string SessionNameMenuID = "MenuID";
        public static string SessionNameUsername = "Username";
        public static string SessionNameFacilities = "Facilities";
        public static string SessionNameProcessDataModel = "ProcessDataModel";
    }
}