﻿using M3Light.Databases;
using M3Light.Models;
using PolisportAPI;

namespace M3Light.Logic
{
    public class LoginLogic
    {
        #region dependencies
        UserRepository userRepo = new UserRepository();
        #endregion

        public bool CheckLogin(string username, string password)
        {
            try
            {
                var UsersAD = new PolisportAPI.LdapAuthentication(PolisportAPI.SystemConfigs.ADLink);
                var temAcessoAD = PolisportAPI.API.CheckADLogin(username.Trim(), password.Trim(), UsersAD);

                if (!temAcessoAD)
                {
                    return false;
                }

                UserPoco poco = null;
                using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs))
                {
                    poco = userRepo.GetUser(conn, username);
                }

                if (poco == null)
                {
                    return false;
                }

                return true;
            } catch
            {
                return false;
            }
        }
    }
}