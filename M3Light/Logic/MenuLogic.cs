﻿using M3Light.Databases;
using M3Light.Models;
using PolisportAPI;
using System.Collections.Generic;

namespace M3Light.Logic {
    public class MenuLogic {
        #region dependencies
        UserMenuPermissionViewRepository userMenuPermissionRepo = new UserMenuPermissionViewRepository();
        MenuFieldsRepository menuFieldsRepo = new MenuFieldsRepository();
        #endregion

        public List<UserPermissionViewPoco> GetMenus (string username) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return userMenuPermissionRepo.GetMenus(conn, username);
            }
        }

        public List<MenuFieldsPoco> GetMenuFields (int menuID) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.LigacaoHannibalPolisportAppsConfigs)) {
                return menuFieldsRepo.GetMenuFields(conn, menuID);
            }
        }
    }
}