﻿using M3Light.Models;
using Newtonsoft.Json;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace M3Light.Logic {
    public class GridLogic {
        #region properties
        private int PageSize { get { return 10; } }
        #endregion

        public GridTableModel FillData (DataTable data, ProcessDataModel model, List<SubMenuModel> subMenus) {
            if (model.Page < 1) {
                model.Page = 1;
            }

            data.CaseSensitive = false;
            var dataView = new DataView(data);

            if (model.Filters.Count > 0) {
                var query = string.Empty;

                foreach (var filter in model.Filters) {
                    if (string.IsNullOrEmpty(filter.Value)) {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(query)) {
                        query += " AND ";
                    }

                    try {
                        if (filter.Value.Contains("<->")) {
                            var splitValue = filter.Value.Split(new string[] { "<->" }, StringSplitOptions.None);
                            var de = splitValue[0].Trim();
                            var ate = splitValue[1].Trim();

                            if (!string.IsNullOrEmpty(de)) {
                                query += " [" + filter.Key.Trim() + "] >='" + de + "'";
                            }

                            if (!string.IsNullOrEmpty(de) && !string.IsNullOrEmpty(ate)) {
                                query += " AND ";
                            }

                            if (!string.IsNullOrEmpty(ate)) {
                                query += " [" + filter.Key.Trim() + "] <='" + ate + "'";
                            }
                        } else {
                            var arrayData = JsonConvert.DeserializeObject<string[]>(filter.Value);

                            query += " [" + filter.Key.Trim() + "] IN ('" + string.Join("','", arrayData.Select(x => x.ToLower().Trim())) + "')";
                        }
                    } catch {
                        try {
                            var date = DateTime.Parse(filter.Value);
                            query += "[" + filter.Key.Trim() + "] = '" + date.ToString("yyyy-MM-dd") + "'";
                        } catch (Exception) {
                            query += " [" + filter.Key.Trim() + "] LIKE '%" + filter.Value.Trim() + "%'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(query)) {
                    dataView.RowFilter = query;
                }
            }

            int skip = (model.Page * PageSize) - PageSize;

            var totalRecord = dataView.Count;
            decimal totalPages;

            if (string.IsNullOrEmpty(model.Sort)) {
                var firstColumn = data.Columns[0].ToString();
                model.Sort = firstColumn;
                model.SortDir = "ASC";
            }

            dataView.Sort = model.Sort + " " + model.SortDir;

            if (dataView.Count > 0 && dataView.Count >= skip) {
                data = dataView.ToTable().AsEnumerable().Skip(skip).Take(PageSize).CopyToDataTable();
                var totalPagesRound = Math.Round((decimal)totalRecord / PageSize, MidpointRounding.AwayFromZero);
                totalPages = totalPagesRound == 0 ? 1 : totalPagesRound;
            } else {
                data = dataView.ToTable();
                model.Page = 0;
                totalPages = 0;
            }

            return new GridTableModel() {
                DataToShow = AddColumns(data, model.ColumnsToShow),
                AllPartialData = AddColumns(dataView.ToTable(), model.ColumnsToShow),
                Page = model.Page,
                TotalPages = (int)totalPages,
                Sort = model.Sort,
                SortDir = model.SortDir,
                Filters = model.Filters,
                MenuFieldsToAdd = model.MenuFieldsToAdd,
                RequiredFilters = model.RequiredFilters,
                MenuFieldsSelected = model.MenuFieldsSelected,
                SubMenus = subMenus,
                IsSubMenu = model.IsSubMenu
            };
        }

        public string GetColumnName (string field, string alias) {
            return field + " AS ''" + alias + "''";
        }

        public XSSFWorkbook Export (DataTable data, List<string> columnsToShow) {
            var workbook = new XSSFWorkbook();
            var sheet = workbook.CreateSheet(CacheSystem.SoftwareTitle);

            #region Header
            var rowHeader = sheet.CreateRow(0);

            for (int j = 0; j < data.Columns.Count; j++) {
                if (columnsToShow.FirstOrDefault(x => x.Contains(data.Columns[j].ColumnName)) != null) {
                    var cell = rowHeader.CreateCell(j);
                    cell.SetCellValue(data.Columns[j].ToString());
                }
            }
            #endregion

            #region Rows
            for (int i = 0; i < data.Rows.Count; i++) {
                var row = sheet.CreateRow(i + 1);

                for (int j = 0; j < data.Columns.Count; j++) {
                    if (columnsToShow.FirstOrDefault(x => x.Contains(data.Columns[j].ColumnName)) != null) {
                        var cell = row.CreateCell(j);
                        cell.SetCellValue(data.Rows[i][j].ToString());
                    }
                }
            }
            #endregion

            return workbook;
        }

        public DataTable AddColumns (DataTable data, List<string> columnsToShow) {
            foreach (DataColumn column in data.Columns) {
                if (columnsToShow.FirstOrDefault(x => x.Contains(column.ColumnName)) == null) {
                    data.Columns[column.ColumnName].ColumnMapping = MappingType.Hidden;
                } else {
                    data.Columns[column.ColumnName].ColumnMapping = MappingType.Element;
                }
            }

            return data;
        }
    }
}