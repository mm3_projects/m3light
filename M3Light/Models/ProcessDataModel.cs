﻿using System.Collections.Generic;

namespace M3Light.Models {
    public class ProcessDataModel {
        public bool RefreshData { get; set; }

        public Dictionary<string, string> RequiredFilters { get; set; }

        public List<string> DatabaseMandatoryColumns { get; set; }

        public string Sort { get; set; }

        public string SortDir { get; set; }

        public int Page { get; set; }

        public Dictionary<string, string> Filters { get; set; }

        public List<string> ColumnsToShow { get; set; }

        public Dictionary<string, string> MenuFieldsToAdd { get; set; }

        public string MenuFieldsSelected { get; set; }

        public List<UserPermissionViewPoco> SubMenus { get; set; }

        public bool IsSubMenu { get; set; }
    }
}