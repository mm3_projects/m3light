﻿using PolisportAPI.Data;

namespace M3Light.Models {
    public class MenuFieldsPoco : IPoco {
        public string PrimaryKey {
            get {
                return "MenuID";
            }
        }

        public bool PrimaryKeyIdentity {
            get {
                return false;
            }
        }

        public int MenuID { get; set; }

        public string Field { get; set; }

        public string Alias { get; set; }

        public string Description { get; set; }

        public bool IsMandatory { get; set; }
    }
}