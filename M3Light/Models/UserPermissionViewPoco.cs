﻿using PolisportAPI.Data;

namespace M3Light.Models {
    public class UserPermissionViewPoco : IPoco {
        public string PrimaryKey {
            get {
                return "MenuID";
            }
        }

        public bool PrimaryKeyIdentity {
            get {
                return false;
            }
        }

        public int MenuID { get; set; }

        public string Menu { get; set; }

        public string M3Menu { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public bool IsEnable { get; set; }

        public int? MenuPrincipalID { get; set; }

        public bool ShowInMainMenu { get; set; }

        public string Username { get; set; }
    }
}