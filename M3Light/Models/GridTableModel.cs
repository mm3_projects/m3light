﻿using System.Collections.Generic;
using System.Data;

namespace M3Light.Models {
    public class DataSetTableModel {
        public DataSet DataToShow { get; set; }

        public Dictionary<string, string> RequiredFilters { get; set; }
    }
}