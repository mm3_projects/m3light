﻿using System.Collections.Generic;

namespace M3Light.Models {
    public class SubMenuModel {
        public int MenuID { get; set; }

        public string MenuName { get; set; }

        public List<string> Filters { get; set; }
    }
}