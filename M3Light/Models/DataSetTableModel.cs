﻿using System.Collections.Generic;
using System.Data;

namespace M3Light.Models {
    public class GridTableModel {
        public DataTable DataToShow { get; set; }

        public DataTable AllPartialData { get; set; }

        public int TotalPages { get; set; }

        public int Page { get; set; }

        public string Sort { get; set; }

        public string SortDir { get; set; }

        public Dictionary<string, string> Filters { get; set; }

        public Dictionary<string, string> RequiredFilters { get; set; }

        public string MenuFieldsSelected { get; set; }

        public Dictionary<string, string> MenuFieldsToAdd { get; set; }

        public List<SubMenuModel> SubMenus { get; set; }

        public bool IsSubMenu { get; set; }
    }
}