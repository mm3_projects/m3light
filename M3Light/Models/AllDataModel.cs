﻿using System.Collections.Generic;
using System.Data;

namespace M3Light.Models {
    public class AllDataModel {
        public DataTable Data { get; set; }

        public List<string> ColumnsToShow { get; set; }
    }
}