﻿using PolisportAPI.Data;

namespace M3Light.Models
{
    public class UserPoco : IPoco
    {
        public string PrimaryKey {
            get {
                return "UserID";
            }
        }

        public bool PrimaryKeyIdentity {
            get {
                return true;
            }
        }

        public int UserID { get; set; }

        public string Username { get; set; }

        public bool IsEnable { get; set; }
    }
}